# WiFiQR for iOS

A small iOS & Swift student project which generates a QR Code to connect to your WiFi network

## Features

<img src="screenshots/wifiqr01.png" width=146 height=316 />
<img src="screenshots/wifiqr02.png" width=146 height=316 />
<img src="screenshots/wifiqr03.png" width=146 height=316 />

- Generate a QR Code you can scan, so you can access your WiFi network quickly
- Support for open networks, as well as WEP, and WPA1/2/3 encryption
- Native light and dark iOS modes
- And that's pretty much it!

## What to expect

Not much at all, really. This app is barebones and may have no actual real-life purpose. It may or may not evolve into a full-fledged project. You have been warned!

## Thanks goes to...

- [Simplon.co](https://simplon.co) for the Apple Foundation Program formation
- [Ajabep](https://gitlab.com/ajabep), my talented wife for the inspiration and so much more!

## About me

The usual stuff.

- [My website](https://ombremad.lesbienn.es) (in french)
- [My Twitter](https://twitter.com/ombremad) (mostly, in french too)

Trans rights! 🏳️‍⚧️
