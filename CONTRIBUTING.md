# How to contribute

## Please DO NOT do pull requests

As this is my student side-project, I intend on developing and improving it myself, as a way to learn how to solve issues and bugs by myself. As imperfect as the code might be, please refrain from doing so for the time being.

Please feel free to fork the project, which is licensed under the quite simple MIT License, if you want to try and add your own twist to it.

## Please DO submit issues

You are, however, more than welcome in submitting issues on GitLab! Your bug reports and feature suggestions both help a great deal in making this a better app.
