//
//  ContentView.swift
//  WiFiQR
//
//  Created by Anne Ferret on 15/06/2022.
//

import SwiftUI

struct MainView: View {
    
    @State var network = Network()
    
    var body: some View {
        
        NavigationView {
            
            Form {
                
                Section {
                    
                    HStack {
                        Image(systemName: "wifi")
                            .font(Font.system(size: 18))
                            .frame(width: CGFloat(frameWidth), height: CGFloat(frameHeight))
                            .foregroundColor(.accentColor)
                        TextField("SSID", text: $network.ssid)
                            .textInputAutocapitalization(.never)
                            .disableAutocorrection(true)
                    }
                    
                    HStack {
                        Image(systemName: "checkerboard.shield")
                            .font(Font.system(size: 18))
                            .frame(width: CGFloat(frameWidth), height: CGFloat(frameHeight))
                            .foregroundColor(.accentColor)
                        Picker("Security type", selection: $network.security) {
                            ForEach(SecurityType.allCases) { security in
                                Text(security.rawValue)
                            }
                        }
                    }
                    
                    if network.security != SecurityType.none {
                        HStack {
                            Image(systemName: "lock.fill")
                                .font(Font.system(size: 18))
                                .frame(width: CGFloat(frameWidth), height: CGFloat(frameHeight))
                                .foregroundColor(.accentColor)
                            SecureField("Password", text: $network.password)
                                .textInputAutocapitalization(.never)
                                .disableAutocorrection(true)
                        }
                    }
                    
                    HStack {
                        Image(systemName: "eye.slash.fill")
                            .font(Font.system(size: 18))
                            .frame(width: CGFloat(frameWidth), height: CGFloat(frameHeight))
                            .foregroundColor(.accentColor)
                        Toggle(isOn: $network.hidden) {
                            Text("Hidden")
                        }
                        .tint(.accentColor)
                    }
                    
                }
                
                Section {
                    NavigationLink(destination: ResultView(network: network)) {
                        HStack {
                            Image(systemName: "checkmark")
                                .font(Font.system(size: 18, weight: .heavy))
                                .frame(width: CGFloat(frameWidth), height: CGFloat(frameHeight))
                            Text("Generate QR Code")
                                .fontWeight(.heavy)
                        }
                    }
                }
                .foregroundColor(Color("PrimaryColor"))
                .listRowBackground(Color.accentColor)
                
            }
            .navigationTitle("WiFiQR")
            .toolbar {
                NavigationLink(destination: InfoView()) {
                    Image(systemName: "info.circle.fill")
                }
            }
            
        }
    }
}


struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
