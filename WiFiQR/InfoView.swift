//
//  InfoView.swift
//  WiFiQR
//
//  Created by Anne Ferret on 08/07/2022.
//

import SwiftUI

struct InfoView: View {
    
    @State private var animatedTransness : Bool = false
    
    var body: some View {
        
        Form {
            
            Section {
                VStack(alignment:.leading) {
                    Link(destination: URL(string: "https://gitlab.com/ombremad/wifiqr-for-ios")!) {
                        HStack {
                            Image(systemName: "link")
                            Text("WiFiQR for iOS")
                                .fontWeight(.semibold)
                        }
                    }
                    Text("on GitLab")
                        .font(.caption)
                        .offset(x:29.5)
                }
            } header : {
                Text("About this project")
            }
            
            Section {
                VStack(alignment:.leading) {
                    Link(destination: URL(string: "https://ombremad.lesbienn.es")!) {
                        HStack {
                            Image(systemName: "link")
                            Text("My website")
                                .fontWeight(.semibold)
                        }
                    }
                    Text("in french")
                        .font(.caption)
                        .offset(x:29.5)
                }
                VStack(alignment:.leading) {
                    Link(destination: URL(string: "https://twitter.com/ombremad")!) {
                        HStack {
                            Image(systemName: "link")
                            Text("My Twitter")
                                .fontWeight(.semibold)
                        }
                    }
                    Text("mostly in french, too")
                        .font(.caption)
                        .offset(x:29.5)
                }
            } header : {
                Text("About me")
            }
            
            Section {
                VStack(alignment:.leading) {
                    Link(destination: URL(string: "https://simplon.co")!) {
                        HStack {
                            Image(systemName: "link")
                            Text("Simplon.co")
                                .fontWeight(.semibold)
                        }
                    }
                    Text("for the Apple Foundation Program")
                        .font(.caption)
                        .offset(x:29.5)
                }
                VStack(alignment:.leading) {
                    Link(destination: URL(string: "https://gitlab.com/ajabep")!) {
                        HStack {
                            Image(systemName: "link")
                            Text("Ajabep")
                                .fontWeight(.semibold)
                        }
                    }
                    Text("my talented wife,\rfor the inspiration and so much more!")
                        .font(.caption)
                        .offset(x:29.5)
                }
            } header : {
                Text("Thanks goes to")
            }
            
            Section {
                NavigationLink(destination: LicenseView()) {
                    Text("License")
                }
                Text("Trans rights! 🏳️‍⚧️")
                    .foregroundColor(
                        animatedTransness ? Color.black : Color("SecondaryColor")
                    )
                    .listRowBackground(
                        LinearGradient(gradient: Gradient(colors: [Color("RowBackgroundColor"), Color("MayaBlue"), Color("AmaranthPink"), Color.white, Color("AmaranthPink"), Color("MayaBlue"), Color("RowBackgroundColor")]), startPoint: animatedTransness ? .top : .bottom, endPoint: .bottom)
                            .animation(.easeInOut(duration: 0.2), value: animatedTransness)
                    )
                    .onTapGesture() {
                        animatedTransness.toggle()
                    }
            } header : {
                Text("Also")
            }
        }
        .navigationBarTitle("Credits & links")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
