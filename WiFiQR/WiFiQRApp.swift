//
//  WiFiQRApp.swift
//  WiFiQR
//
//  Created by Anne Ferret on 15/06/2022.
//

import SwiftUI

@main
struct WiFiQRApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
