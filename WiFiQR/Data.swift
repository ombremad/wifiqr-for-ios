//
//  Data.swift
//  WiFiQR
//
//  Created by Anne Ferret on 15/06/2022.
//

import SwiftUI
import CoreImage.CIFilterBuiltins       // for QR code generation


// QR code generation

let context = CIContext()
let filter = CIFilter.qrCodeGenerator()

func generateQRCode(from string: String) -> UIImage {
    filter.message = Data(string.utf8)

    if let outputImage = filter.outputImage {
        if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
            return UIImage(cgImage: cgimg)
        }
    }

    return UIImage(systemName: "xmark.circle") ?? UIImage()
}


// save image

func saveImage(from string: String) {
    let savedImage : UIImage? = generateQRCode(from: string)

    let imageSaver = ImageSaver()
    if let savedImage = savedImage {
        imageSaver.writeToPhotoAlbum(image: savedImage)}
}

class ImageSaver: NSObject {
    func writeToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveCompleted), nil)
    }

    @objc func saveCompleted(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Save finished!")
    }
}


// Default frame values

let frameWidth:Int = 25
let frameHeight:Int = 50


// Data model

struct Network {
    var ssid : String = ""
    var security : SecurityType = SecurityType.none
    var password : String = ""
    var hidden : Bool = false
    var id: Self { self }
}

enum SecurityType : String, CaseIterable, Identifiable {
    case none = "Open"
    case wep = "WEP"
    case wpa = "WPA / WPA2-PSK"
    case sae = "WPA3-PSK"
    var id: Self { self }
}
