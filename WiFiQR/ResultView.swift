//
//  ResultView.swift
//  WiFiQR
//
//  Created by Anne Ferret on 15/06/2022.
//

import SwiftUI

struct ResultView: View {
    
    @State var network : Network
    
    var body: some View {
        
        let qrCodeResult:String = "WIFI:S:\(network.ssid);T:\(network.security);P:\(network.password);\(network.hidden ? "H:true;" : "");"
        
        Form {
            
            Section {
                HStack {
                    Spacer()
                    Image(uiImage: generateQRCode(from:qrCodeResult))
                        .interpolation(.none)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 300, height: 300)
                    Spacer()
                }
            }
            
            Section {
                VStack(alignment: .leading) {
                    Text("SSID")
                        .font(.caption)
                    Text(network.ssid)
                        .fontWeight(.medium)
                    if network.hidden == true {
                        Text("is hidden")
                            .font(.footnote)
                    }
                }
                VStack(alignment: .leading) {
                    Text("Security")
                        .font(.caption)
                    Text(network.security.rawValue)
                        .fontWeight(.medium)
                }
                if network.password.isEmpty {} else {
                    VStack(alignment: .leading) {
                        Text("Password")
                            .font(.caption)
                        Text(network.password)
                            .fontWeight(.medium)
                    }
                }
            } header : {
                Text("Detailed informations")
            }
            
        }
        .navigationTitle("QR Code")
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            Button (
                action: {
                    saveImage(from: qrCodeResult)
                },
                label: {
                    Image(systemName: "square.and.arrow.down.on.square")
                })
        }
        
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
            ResultView(network: Network())
    }
}
